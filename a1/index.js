
// Exponent Operator
	
	const num = 2
	const firstNum = num**3;
	// console.log(firstNum);

	console.log(`The cube of ${num} is ${firstNum}.`);
	

// Template Literals

	const address = ["5", "Main St", "New York City", "New York"];
	const [streetNum, streetName, city, state] = address;
	console.log(`I live in ${streetNum} ${streetName} ${city} ${state}.`);


// Object Destructuring
	const myPet = {
		petname: "Lolong",
		species: "saltwater crocodile",
		weight: "1075",
		length: "20 ft 3 in"
	}

	const {petname, species, weight, length} = myPet;

	console.log(`${petname} was a ${species}. He weighed at ${weight} kgs with a measurement of ${length}.`);




// For Each
	const numbers = [1,2,3,4,5];

	numbers.forEach((number)=>{
		console.log(number);
	});


// Reduce

	const initialValue = 0;
	const reduceNumber = numbers.reduce(
 		 (previousValue, currentValue) => previousValue + currentValue,
 		 initialValue
			);

		console.log(reduceNumber);


// Constructor

	class Dog {
		constructor(name,age,breed){
			this.name = name;
			this.age = age;
			this.breed = breed;
			
		}
	}

	const dogProfile = new Dog("Basty", 5, "Shih Tzu");


	console.log(dogProfile);
