

	// Exponent Operator

	const firstNum = 8**2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);

	let string1 = "fun";
	let string2 = "Bootcamp";
	let string3 = "Coding";
	let string4 = "Javascript";
	let string5 = "Zuitt";
	let string6 = "Learning";
	let string7 = "Love";
	let string8 = "I";
	let string9 = "is";
	let string10 = "in";

	// let sentence1 = (string8 + " " + string7 + " " + string6 + " " + string4 + ".");
	// let sentence2 = (string6 + " " + string10 + " " + string2 + " " + string9 + " " + string1 + ".");
	// console.log(sentence1);
	// console.log(sentence2);

	// Template Literals

	let sentence1 = `${string8} ${string7} ${string6} ${string4}`;
	console.log(sentence1);

	let name = "John";

	let message = "Hello" + name + "! Welcome to programming!";
	console.log(message);

	message = `Hello" ${name} "! Welcome to programming!`;
	console.log(message);

	// Multi-Line using Template Literals

	const anotherMessage =`
	${name} attended a math competition.
	He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.

	`
	console.log(anotherMessage);

	let dev = {
		name: "Peter",
		lastName: "Parker",
		occupation: "Web Developer",
		income: 50000,
		expenses: 60000
	}

	console.log(`${dev.name} is a ${dev.occupation}`);
	console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`)

	const interestRate = .1;
	const principal = 1000;

	console.log(`The interest on you savings account is ${principal * interestRate}.`);

// Array Destructuring

	const fullName = ["Juan", "Dela", "Cruz"];

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! `);

// Reaassign Array --->

	const [firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

		console.log(`Hello ${firstName} ${middleName} ${lastName}! `);

// Object Destructuring

	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	}

	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	const {givenName, maidenName, familyName} = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

// Arrow Functions

	const hello = () => {
		console.log("Hello World");
	}

	hello();

	const printFullName = (firstName, middleInitial, lastName) => {
		console.log(`${firstName} ${middleInitial} ${lastName}`);
	}

	printFullName("John", "D","Smith");

	const students = ["John", "Jane", "Judy"];

	students.forEach(function(student){
		console.log(`${student} is a student`);
	});

	students.forEach((student)=>{
		console.log(`${student} is a student`);
	});

// Implicit Return Statement

	const subtract = (x,y) => x - y;
	const multiply = (x,y) => x * y;
	const divide = (x,y) => x / y;

	let diff = subtract(1,2);
	let product = multiply(1,2);
	let quotient = divide(1,2);
	console.log(diff);
	console.log(product);
	console.log(quotient);

// Defaul Function Argument Value

	const greet = (name = 'User') => {
		return `"Good evening", ${name}`;
	}
	console.log(greet());
	console.log(greet("John"));

// Class-based Object Blueprint

// Create class

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = brand;
			this.year = year;
		}
	}

	const myCar = new Car();

	console.log(myCar);

	myCar.brand = "Ford";
	myCar.name = "Everest";
	myCar.year = "1996";

	console.log(myCar);

	const myNewCar = new Car("Toyota", "Vios", 2001);
	console.log(myNewCar);

	class hunter {
		constructor(hero,role,strength,weakness){
			this.hero = hero;
			this.role = role;
			this.strength = strength;
			this.weekness = weakness;
		}
	}

	const hero1 = new hunter("Gon freecss", "Enhancer", "Creativity", "Impulse Control");
	const hero2 = new hunter("Killua Zoldyck", "Transmuter", "Incredibly High Pain Tolerance", "Naturally Pessimistic");

	console.log(hero1);
	console.log(hero2);

